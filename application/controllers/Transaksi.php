<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends CI_Controller {

	  public function __construct()
	  {
	    parent::__construct();
	    $this->load->model('model_transaksi','tx');
	  }

	  public function index()
	  {
	      $data['main_view']='view_transaksi';
	      $data['tampil_buku']=$this->tx->getDataBukuTrans();
	      $this->load->view('template', $data);
	  }

	  public function addcart($id)
	  {
	    $detail=$this->tx->detail($id);
	    $data = array(
	        'id'      => $detail->kode_buku,
	        'qty'     => 1,
	        'price'   => $detail->harga,
	        'name'    => $detail->judul_buku,
	        'options' => array('genre'=>$detail->nama_kategori,'stok'=>$detail->stok)
	      );
	    $this->cart->insert($data);     
	    redirect('transaksi');
	  }

	  public function hapuscart($id)
	  {
	    $data = array(
	      'rowid' => $id,
	      'qty'   => 0
	    );
	    
	    $this->cart->update($data);
	    redirect('transaksi');
	  }

	  public function ubahqty($id)
	  {
	    $data = array(
	      'rowid' => $id,
	      'qty'   => $this->input->post('qty')
	    );
	    
	    $this->cart->update($data);
	    redirect('transaksi');
	  }

	  public function checkout()
	  { 
	    $kembalian = $this->input->post('uang') - $this->cart->total();
	    if ($this->input->post('uang')<$this->cart->total()) {
	      $this->session->set_flashdata('pesan', 'Uangnya kurang Bois!');
	    }
	    else{
	      if ($this->tx->simpanTrans()) {
	        $lastTrans=$this->tx->lastTrans()->kode_transaksi;
	        $this->session->set_flashdata('pesan', '<h4>kembali : Rp.'.$kembalian.'</h4>');
	        $this->session->set_flashdata('pesan_print', 
	          'Tekan <a href="nota/cetak/'.$lastTrans.'">disini</a> untuk mencetak Nota!');
	        $this->cart->destroy();     
	      }
	      else{
	        $this->session->set_flashdata('pesan', 'error');
	      }
	    }
	    redirect('transaksi');
	  }
  
}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */