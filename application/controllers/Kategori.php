<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_kategori', 'ktg');
	}

	public function index()
	{
		$data['tampil_kategori']=$this->ktg->tampil_ktg();
		$data['main_view']="view_kategori";
		$data['judul']="Kategori";
		$this->load->view('template', $data);
	}

	public function edit_kategori($id)
	{
		$data=$this->ktg->detail($id);
		echo json_encode($data);
	}

	public function tambah()
	{
		if($this->ktg->simpan_ktg()){
			$this->session->set_flashdata('notif', 'Tambah Kategori Sukses Bois!');
			redirect('kategori');
		} else {
			$this->session->set_flashdata('notif', 'Gagal Menambah Kategori Bois!');
			redirect('kategori');
		}
	}

	public function hapus($id='')
	{
		if($this->ktg->hapus_ktg($id)){
			$this->session->set_flashdata('notif', 'Sukses Hapus Kategori Bois!');
			redirect('kategori');
		} else {
			$this->session->set_flashdata('notif', 'Gagal Hapus Kategori Bois!');
			redirect('kategori');	
		}
	}

	public function kategori_update()
	{
			if($this->ktg->edit_ktg()){
			$this->session->set_flashdata('notif', 'Sukses Edit Kategori Bois!');
			redirect('kategori');
		} else {
			$this->session->set_flashdata('notif', 'Gagal Hapus Kategori Bois!');
			redirect('kategori');	
		}
	}

}

/* End of file Kategori.php */
/* Location: ./application/controllers/Kategori.php */