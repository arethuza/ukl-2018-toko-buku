<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_user');
		$this->load->model('model_buku');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			$data['buku'] = $this->model_buku->getDataBuku();
			$data['kategori'] = $this->model_buku->getKategori();
			$data['main_view'] = 'view_buku';
			$this->load->view('template', $data);
		} else {
			redirect('login');
		}
	}

	public function simpan() {
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']  = '2000';

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('foto_cover')) 
		{

			if ($this->model_buku->simpan($this->upload->data()) == TRUE) 
			{

				$data['buku'] = $this->model_buku->getDataBuku();
				$data['main_view'] = 'view_buku';
				$this->session->set_flashdata('notif', 'Tambah buku berhasil bois!');
				$this->load->view('template', $data);
				
			} else 
			{
				$data['buku'] = $this->model_buku->getDataBuku();
				$data['main_view'] = 'view_buku';
				$this->session->set_flashdata('notif', 'Tambah buku gagal bois!');
				$this->load->view('template', $data);
			}

		} else 
		{
			$data['buku'] = $this->model_buku->getDataBuku();
			$data['main_view'] = 'view_buku';
			$data['notif'] = $this->upload->display_errors();
			$this->load->view('template', $data);
		}
	}

	public function hapus($id)
	{
		if($this->model_buku->delete($id) == TRUE){
			$this->session->set_flashdata('notif', 'Buku Berhasil Dihapus Bois!');
			redirect('buku');
		} else {
			$this->session->set_flashdata('notif', 'Buku Gagal Dihapus Bois!');
			redirect('buku');
		}
	}

	public function edit($id)
	{
		if($this->model_buku->edit($id) == TRUE){
			$this->session->set_flashdata('notif', 'Edit data berhasil bois!');
			redirect('buku');
		} else {
			$this->session->set_flashdata('notif', 'Edit data gagal bois!');
            redirect('buku');
		}
	}

	public function detil($id)
	{
		$data['detail'] = $this->model_buku->getDetailBuku($id)->result_object();
	}

}

/* End of file Buku.php */
/* Location: ./application/controllers/Buku.php */