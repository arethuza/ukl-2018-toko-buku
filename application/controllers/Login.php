<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_user');
	}

	public function index()
	{
		if($this->session->userdata('logged_in') == TRUE){
			redirect('login/dashboard');
		} else {
			$this->load->view('view_login');
		}
	}

	public function dashboard() {
		if ($this->session->userdata('logged_in') == TRUE) {
			$data['main_view'] = 'dashboard';
			$this->load->view('template', $data);
		} else {
			redirect('login');
		}
	}

	public function testd() {
		$this->load->view('template');
	}

	public function do_logon() {
		if ($this->model_user->cek_user() == TRUE) {
			redirect('login/dashboard');
		} else {
			$data['notif'] = 'Login gagal bois !';
			$this->load->view('view_login', $data);
		}
	}

	public function logout() {
		$data = array('username' => '', 'logged_in' => FALSE);
		$this->session->sess_destroy();
		$this->load->view('view_login');
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */