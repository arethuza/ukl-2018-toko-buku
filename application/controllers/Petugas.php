<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_petugas', 'poi');
	}

	public function index()
	{
		$data['petugas'] = $this->poi->getDataPetugas();
		$data['main_view'] = 'view_petugas';
		$this->load->view('template', $data);
	}

	public function simpan()
	{
		if($this->poi->simpan() == TRUE){
			$this->session->set_flashdata('notif1', 'Petugas Berhasil Ditambahkan Bois!');
            redirect('petugas',$data);
		} else {
			$this->session->set_flashdata('notif1', 'Petugas Gagal Ditambahkan Bois!');
            redirect('petugas',$data);
		}
	}

	public function hapus($id)
	{
		if($this->poi->delete($id) == TRUE){
			$this->session->set_flashdata('notif', 'Data Petugas Berhasil Dihapus Bois!');
			redirect('petugas');
		} else {
			$this->session->set_flashdata('notif', 'Data Petugas Gagal Dihapus Bois!');
			redirect('petugas');
		}
	}

	public function edit($id)
	{
		if($this->poi->edit($id) == TRUE){
			$this->session->set_flashdata('notif', 'Edit data berhasil Bois!');
			redirect('petugas');
		} else {
			$this->session->set_flashdata('notif', 'Edit data berhasil Bois!');
            redirect('petugas');
		}
	}

	public function detil($id)
	{
		$data['detail'] = $this->poi->getDetailPetugas($id)->result_object();
	}

}

/* End of file Petugas.php */
/* Location: ./application/controllers/Petugas.php */