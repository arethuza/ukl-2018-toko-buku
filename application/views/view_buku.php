           <div class="table-agile-info">
            
                    <section class="panel">
                        <header class="panel-heading">
                            <h2>BUAT BUKU</h2>
                        </header>
                            <?php
                                if(!empty($notif)){
                                    echo '<div class="alert alert-danger">';
                                    echo $notif;
                                    echo '</div>';
                                }

                                if ($this->session->userdata('level')=="kasir") {
                                    redirect('login/dashboard','refresh');
                                }
                            ?>
                        <div class="panel-body">
                            <div class="position-center">
                                <form method="post" action="<?php echo base_url(); ?>index.php/buku/simpan" id="form-buku" enctype="multipart/form-data">
                                <div class="form-group">
                                    <?php
                                        $notif = $this->session->flashdata('notif');

                                        if(!empty($notif)){
                                        echo '<div class="alert alert-success">'.$notif.'</div>';
                                        }
                                    ?>
                                    <label for="judul_buku">Title</label>
                                    <input type="text" class="form-control" name="judul_buku" placeholder="Judul Buku..." required>
                                </div>
                                <div class="form-group">
                                    <label for="tahun">Year</label>
                                    <input type="text" class="form-control" name="tahun" placeholder="Tahun..." required>
                                </div>
                                <div class="form-group">
                                    <label for="tahun">Category</label>
                                    <select class="form-control" name="kategori">
                                          <?php foreach ($kategori as $a): ?>
                                        <option value="<?=$a->kode_kategori?>"><?=$a->nama_kategori?></option>
                                        <?php endforeach?>
                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label for="harga">Price</label>
                                    <input type="text" class="form-control" name="harga" placeholder="Harga..." required>
                                </div>
                                 <div class="form-group">
                                    <label for="penerbit">Penerbit</label>
                                    <input type="text" class="form-control" name="penerbit" placeholder="Penerbit..." required>
                                </div>
                                 <div class="form-group">
                                    <label for="penulis">Author</label>
                                    <input type="text" class="form-control" name="penulis" placeholder="Penulis..." required>
                                </div>
                                 <div class="form-group">
                                    <label for="stok">Stok</label>
                                    <input type="text" class="form-control" name="stok" placeholder="Stok..." required>
                                </div>
                                <div class="form-group">
                                    <label for="penulis">Photo</label>
                                    <input type="file" class="form-control" name="foto_cover" required>
                                </div>
                                <div class="row">
                                <div class="col-lg-6">
                                    <input type="reset" name="reset" value="RESET" class="btn btn-info">
                                    <input type="submit" name="submit" value="SAVE" class="btn btn-primary">
                                </div>
                                
                                </div>
                            </form>
                            </div>

                        </div>
                    </section>
<br>
 <div class="panel panel-default">
    <div class="panel-heading">
     <h3>DATA BUKU</h3>
    </div>
    
    <div>
      <table class="table" ui-jq="footable" ui-options='{
        "paging": {
          "enabled": true
        },
        "filtering": {
          "enabled": true
        },
        "sorting": {
          "enabled": true
        }}'>
        <thead>
          <tr>
            <th>No</th>
            <th>Judul Buku</th>
            <th>Tahun</th>
            <th>Kategori</th>
            <th>Harga</th>
            <th>Penerbit</th>
            <th>Penulis</th>
            <th>Stok</th>
            <th>Foto Cover</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
             <?php
                                        $no = 1;
                                        foreach ($buku as $data) {
                                        echo "
                                            <tr class='odd gradeX'>
                                            <td>".$no++."</td>
                                            <td>$data->judul_buku</td>
                                            <td>$data->tahun</td>
                                            <td>$data->nama_kategori</td>
                                            <td>Rp.$data->harga</td>
                                            <td>$data->penerbit</td>
                                            <td>$data->penulis</td>
                                            <td>$data->stok Buku</td>
                                            <td><img width='100pxs' src='".base_url()."/uploads/".$data->foto_cover."''></td>
                                            <td>
                                            <button data-toggle='modal' data-target='#modal$data->kode_buku' class='glyphicon glyphicon-edit btn btn-success'>
                                            </button>
                                            <a href='".base_url()."index.php/buku/hapus/$data->kode_buku' class='glyphicon glyphicon-trash btn btn-danger'>
                                            </a>
                                            </td>
                                            </tr>

                                            <!-- Modal -->
                                        <div class='modal fade' id='modal$data->kode_buku' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
                                            <div class='modal-dialog'>
                                                <div class='modal-content'>
                                                  <form role='form' action='".base_url()."index.php/buku/edit/".$data->kode_buku."' method='post'>
                                                    <div class='modal-header'>
                                                        <button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button>
                                                        <h4 class='modal-title' id='myModalLabel'>Edit Data Buku</h4>
                                                    </div>
                                                    <div class='modal-body'>";?>
                                                      <div class="form-group">
                                                        <label>Title</label>
                                                        <input class="form-control" type="text" name="judul_buku" required value="<?php echo $data->judul_buku; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Year</label>
                                                        <input class="form-control" type="text" name="tahun" required value="<?php echo $data->tahun; ?>">
                                                      </div>

                                                     <div class="form-group">
                                                    <label>Category</label>
                                                    <select class="form-control" name="kategori">
                                                   
                                                     <?php foreach ($kategori as $a): ?>
                                                        <option value="<?=$a->kode_kategori?>"><?=$a->nama_kategori?></option>
                                                    <?php endforeach?>
                                                      
                                                    </select>
                                                    </div>
                                                      <div class="form-group">
                                                        <label>Price</label>
                                                        <input class="form-control" type="text" name="harga" required value="<?php echo $data->harga; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Penerbit</label>
                                                        <input class="form-control" type="text" name="penerbit" required value="<?php echo $data->penerbit; ?>">
                                                      </div>
                                                      <div class="form-group">
                                                        <label>Author</label>
                                                        <input class="form-control" type="text" name="penulis" required value="<?php echo $data->penulis; ?>">
                                                      </div>
                                                       <div class="form-group">
                                                        <label>Stok</label>
                                                        <input class="form-control" type="text" name="stok" required value="<?php echo $data->stok; ?>">
                                                      </div>
                                                    <?php echo "</div>
                                                    <div class='modal-footer'>
                                                    <div class='row'>
                                                    <div class='col-lg-6'>
                                                    <input type='button' name='cancel' class='btn btn-danger btn-block' data-dismiss='modal' value='Cancel'>
                                                    </div>
                                                    <div class='col-lg-6'>
                                                    <input type='submit' name='submit' class='btn btn-success btn-block' value='Edit'>
                                                    </div>
                                                </div>

                                                    </form>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->

                                      ";
                                    }
                                  ?>
        </tbody>
      </table>
    </div>
  </div>
</form>
</div>
<?php
    if(!empty($notif)){
        $base = base_url();
        echo ('
            <script type="text/javascript">
                setInterval(function() {
                    window.location.href="'.$base.'index.php/buku";
                }, 1000);
            </script>
        ');
    }
?>
