<div class="row">
                  <div class="col-lg-9 main-chart" style="color: #333;">
                      <div class="row mt" style="color: #333;">
                        <?php if ($this->session->userdata('level')=="kasir"): ?>
                        <a href="<?php echo base_url(); ?>index.php/transaksi">
                          <div class="col-md-4 col-sm-4 mb" style="color: #333;">
                          <div class="white-panel pn" style="color: #333;">
                            <div class="white-header" style="color: #333;">
                    <h5>BELI BUKU</h5>
                            </div>
                <div class="row">
                  <div class="col-sm-6 col-xs-6 goleft">
                    <p><i class="fa fa-heart"></i> 122</p>
                  </div>
                  <div class="col-sm-6 col-xs-6"></div>
                            </div>
                            <div class="centered">
                    <img src="<?php echo base_url(); ?>assets/img/product.png" width="120">
                            </div>
                          </div>
                        </div><!-- /col-md-4 -->
                        </a>
                      	<?php endif ?>

            <?php if ($this->session->userdata('level')=="admin"): ?>
						<a href="<?php echo base_url(); ?>index.php/petugas"><div class="col-md-4 mb">
							<!-- WHITE PANEL - TOP USER -->
							<div class="white-panel pn" style="color: #333;">
								<div class="white-header" style="color: #333;">
									<h5>TOP USER</h5>
								</div>
								<p><img src="<?php echo base_url(); ?>assets/img/ui-zac.jpg" class="img-circle" width="80"></p>
								<p><b>Ahmad Fajril Dewantara</b></p>
								<div class="row">
									<div class="col-md-6">
										<p class="small mt" style="color: #333;">MEMBER SINCE</p>
										<p>2012</p>
									</div>
									<div class="col-md-6">
										<p class="small mt" style="color: #333;">TOTAL SPEND</p>
										<p>$ 47,60</p>
									</div>
								</div>
							</div>
						</div><!-- /col-md-4 --></a><?php endif ?>

            <div class="col-md-4 mb">
              <!-- INSTAGRAM PANEL -->
              <div class="instagram-panel pn">
                <i class="fa fa-instagram fa-4x"></i>
                <p>@Natash_Ardhia<br/>
                  5 min. ago
                </p>
                <p><i class="fa fa-comment"></i> 200k | <i class="fa fa-heart"></i> 499k</p>
              </div>
            </div><!-- /col-md-4 -->

                    </div><!-- /row -->
                  </div><!-- /col-lg-9 END SECTION MIDDLE -->
                  
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  <?php if ($this->session->userdata('level')=="admin"): ?>
                  <div class="col-lg-3 ds" style="background-color: #f1f1f1;">
                    <!--COMPLETED ACTIONS DONUTS CHART-->
						      <h3>NOTIFICATIONS</h3>
                                        
                      <!-- First Action -->
                      <div class="desc">
                      	<div class="thumb">
                      		<span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                      	</div>
                      	<div class="details">
                      		<p><muted>2 Minutes Ago</muted><br/>
                      		   <a href="#" style="color: #517DFF">Soden Dewantara</a> subscribed to your newsletter.<br/>
                      		</p>
                      	</div>
                      </div>
                      <!-- Second Action -->
                      <div class="desc">
                      	<div class="thumb">
                      		<span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                      	</div>
                      	<div class="details">
                      		<p><muted>3 Hours Ago</muted><br/>
                      		   <a href="#" style="color: #517DFF">Yusuf Surya</a> purchased a year subscription.<br/>
                      		</p>
                      	</div>
                      </div>
                      <!-- Third Action -->
                      <div class="desc">
                      	<div class="thumb">
                      		<span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                      	</div>
                      	<div class="details">
                      		<p><muted>7 Hours Ago</muted><br/>
                      		   <a href="#" style="color: #517DFF">Andrian Rachmat</a> purchased a year subscription.<br/>
                      		</p>
                      	</div>
                      </div>
                      <!-- Fourth Action -->
                      <div class="desc">
                      	<div class="thumb">
                      		<span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                      	</div>
                      	<div class="details">
                      		<p><muted>11 Hours Ago</muted><br/>
                      		   <a href="#" style="color: #517DFF">Mark ZUCC</a> commented your post.<br/>
                      		</p>
                      	</div>
                      </div>
                      <!-- Fifth Action -->
                      <div class="desc">
                      	<div class="thumb">
                      		<span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                      	</div>
                      	<div class="details">
                      		<p><muted>18 Hours Ago</muted><br/>
                      		   <a href="#" style="color: #517DFF">Mendem</a> purchased a wallet in your store.<br/>
                      		</p>
                      	</div>
                      </div>

                       <!-- USERS ONLINE SECTION -->
						<h3>TEAM MEMBERS</h3>
                      <!-- First Member -->
                      <div class="desc">
                      	<div class="thumb">
                      		<img class="img-circle" src="<?php echo base_url(); ?>assets/img/friends/fr-10.jpg" width="35px" height="35px" align="">
                      	</div>
                      	<div class="details">
                      		<p><a href="#" style="color: #517DFF">NATASHA ARDHIA</a><br/>
                      		   <muted>Available</muted>
                      		</p>
                      	</div>
                      </div>
                      <div class="desc">
                        <div class="thumb">
                          <img class="img-circle" src="<?php echo base_url(); ?>assets/img/ui-sam.jpg" width="35px" height="35px" align="">
                        </div>
                        <div class="details">
                          <p><a href="#" style="color: #517DFF">ARETHUZA</a><br/>
                             <muted>I am Busy</muted>
                          </p>
                        </div>
                      </div>
                      <!-- Second Member -->
                      <div class="desc">
                      	<div class="thumb">
                      		<img class="img-circle" src="<?php echo base_url(); ?>assets/img/friends/fr-03.jpg" width="35px" height="35px" align="">
                      	</div>
                      	<div class="details">
                      		<p><a href="#" style="color: #517DFF">AHMAD FAJRIL</a><br/>
                      		   <muted>I am Busy</muted>
                      		</p>
                      	</div>
                      </div>
                      <div class="desc">
                        <div class="thumb">
                          <img class="img-circle" src="<?php echo base_url(); ?>assets/img/friends/fr-02.jpg" width="35px" height="35px" align="">
                        </div>
                        <div class="details">
                          <p><a href="#" style="color: #517DFF">TE</a><br/>
                             <muted>I am Busy</muted>
                          </p>
                        </div>
                      </div>
                      <div class="desc">
                        <div class="thumb">
                          <img class="img-circle" src="<?php echo base_url(); ?>assets/img/ui-sherman.jpg" width="35px" height="35px" align="">
                        </div>
                        <div class="details">
                          <p><a href="#" style="color: #517DFF">KARDIENO YSMT</a><br/>
                             <muted>I am Busy</muted>
                          </p>
                        </div>
                      </div>
                  </div><!-- /col-lg-3 -->
              </div><! --/row --><?php endif ?>








              <?php if ($this->session->userdata('level')=="kasir"): ?>
                  <div class="col-lg-3 ds" style="background-color: #f1f1f1;">
                    <!--COMPLETED ACTIONS DONUTS CHART-->
                  <h3>NOTIFICATIONS</h3>
                                        
                      <!-- First Action -->
                      <div class="desc">
                        <div class="thumb">
                          <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                        </div>
                        <div class="details">
                          <p><muted>2 Minutes Ago</muted><br/>
                             <a href="#" style="color: #517DFF">Soden Dewantara</a> subscribed to Tokobuku Nigger newsletter.<br/>
                          </p>
                        </div>
                      </div>
                      <!-- Second Action -->
                      <div class="desc">
                        <div class="thumb">
                          <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                        </div>
                        <div class="details">
                          <p><muted>3 Hours Ago</muted><br/>
                             <a href="#" style="color: #517DFF">Yusuf Surya</a> purchased a year subscription from Tokobuku Nigger.<br/>
                          </p>
                        </div>
                      </div>
                      <!-- Third Action -->
                      <div class="desc">
                        <div class="thumb">
                          <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                        </div>
                        <div class="details">
                          <p><muted>7 Hours Ago</muted><br/>
                             <a href="#" style="color: #517DFF">Andrian Rachmat</a> purchased a year subscription from Tokobuku Nigger.<br/>
                          </p>
                        </div>
                      </div>
                      <!-- Fourth Action -->
                      <div class="desc">
                        <div class="thumb">
                          <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                        </div>
                        <div class="details">
                          <p><muted>11 Hours Ago</muted><br/>
                             <a href="#" style="color: #517DFF">Mark ZUCC</a> commented on Tokobuku Nigger's post.<br/>
                          </p>
                        </div>
                      </div>
                      <!-- Fifth Action -->
                      <div class="desc">
                        <div class="thumb">
                          <span class="badge bg-theme"><i class="fa fa-clock-o"></i></span>
                        </div>
                        <div class="details">
                          <p><muted>18 Hours Ago</muted><br/>
                             <a href="#" style="color: #517DFF">Mendem</a> purchased a wallet from Tokobuku Nigger store.<br/>
                          </p>
                        </div>
                      </div>

                       <!-- USERS ONLINE SECTION -->
                    <h3>ADMINISTRATOR</h3>
                      <!-- First Member -->
                      <div class="desc">
                        <div class="thumb">
                          <img class="img-circle" src="<?php echo base_url(); ?>assets/img/friends/fr-10.jpg" width="35px" height="35px" align="">
                        </div>
                        <div class="details">
                          <p><a href="#" style="color: #517DFF">NATASHA ARDHIA</a><br/>
                             <muted>Available</muted>
                          </p>
                        </div>
                      </div>
                      <div class="desc">
                        <div class="thumb">
                          <img class="img-circle" src="<?php echo base_url(); ?>assets/img/ui-sam.jpg" width="35px" height="35px" align="">
                        </div>
                        <div class="details">
                          <p><a href="#" style="color: #517DFF">ARETHUZA</a><br/>
                             <muted>I am Busy</muted>
                          </p>
                        </div>
                      </div>
                      <!-- Second Member -->
                      <div class="desc">
                        <div class="thumb">
                          <img class="img-circle" src="<?php echo base_url(); ?>assets/img/friends/fr-03.jpg" width="35px" height="35px" align="">
                        </div>
                        <div class="details">
                          <p><a href="#" style="color: #517DFF">AHMAD FAJRIL</a><br/>
                             <muted>I am Busy</muted>
                          </p>
                        </div>
                      </div>
                      <div class="desc">
                        <div class="thumb">
                          <img class="img-circle" src="<?php echo base_url(); ?>assets/img/friends/fr-02.jpg" width="35px" height="35px" align="">
                        </div>
                        <div class="details">
                          <p><a href="#" style="color: #517DFF">TE</a><br/>
                             <muted>I am Busy</muted>
                          </p>
                        </div>
                      </div>
                      <div class="desc">
                        <div class="thumb">
                          <img class="img-circle" src="<?php echo base_url(); ?>assets/img/ui-sherman.jpg" width="35px" height="35px" align="">
                        </div>
                        <div class="details">
                          <p><a href="#" style="color: #517DFF">KARDIENO YSMT</a><br/>
                             <muted>I am Busy</muted>
                          </p>
                        </div>
                      </div>
                  </div><!-- /col-lg-3 -->
              </div><! --/row --><?php endif ?>