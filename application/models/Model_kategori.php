<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kategori extends CI_Model {

	public function tampil_ktg() {
		$tm_kategori=$this->db->get('data_kategori_buku')->result();
		return $tm_kategori;
	}

	public function detail($a) {
		return $this->db->where('kode_kategori',$a)
						->get('data_kategori_buku')
						->row();
	}

	public function simpan_ktg() {
		$object=array(
			'kode_kategori'=>$this->input->post('kode_kategori'),
			'nama_kategori'=>$this->input->post('nama_kategori'),
		);
		return $this->db->insert('data_kategori_buku', $object);
	}

	public function hapus_ktg($id='')
	{
		return $this->db->where('kode_kategori',$id)->delete('data_kategori_buku');
	}

	public function edit_ktg()
	{
		$object=array(
			'nama_kategori'=>$this->input->post('nama_kategori')
		);
		return $this->db->where('kode_kategori',$this->input->post('id_kategori_lama'))->update('data_kategori_buku', $object);
	}

}

/* End of file Model_kategori.php */
/* Location: ./application/models/Model_kategori.php */