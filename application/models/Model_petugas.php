<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_petugas extends CI_Model {

	public function getDataPetugas(){
		return $this->db->order_by('kode_user','ASC')->get('data_user')->result();
	}

	public function simpan()
	{
		$data = array(
				'username'	=> $this->input->post('username'),
				'password'	=> $this->input->post('password'),
				'nama_user'	=> $this->input->post('nama_user'),
				'level'		=> $this->input->post('level')
			);

		$this->db->insert('data_user',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function edit($id)
	{
		$data = array(
				'username'	=> $this->input->post('username'),
				'password'	=> $this->input->post('password'),
				'nama_user'	=> $this->input->post('nama_user'),
				'level'		=> $this->input->post('level')
			);

		$this->db->where('kode_user',$id)->update('data_user',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete($id)
	{
		$this->db->where('kode_user',$id)
				 ->delete('data_user');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function getDetailPetugas($id)
	{
		return $this->db->get_where('user',array('kode_user'=>$id));
	}

}

/* End of file Model_petugas.php */
/* Location: ./application/models/Model_petugas.php */