<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {

	public function cek_user() {
		$username = $this -> input -> post('username');
		$password = $this -> input -> post('password');

		$query = $this->db->where('username', $username)
						  ->where('password', $password)
						  ->get('data_user');

		if ($query -> num_rows() > 0) {
			$user = array_shift($query -> result_array());
			$data = array(
						'username' => $username,
						'logged_in' => TRUE,
						'id' => $user['kode_user'],
						'nama_user' => $user['nama_user'],
						'level' => $user['level']
					);
			
			$this->session->set_userdata( $data );

			return TRUE;
		} else {
			return FALSE;
		}
	}

}

/* End of file Model_user.php */
/* Location: ./application/models/Model_user.php */