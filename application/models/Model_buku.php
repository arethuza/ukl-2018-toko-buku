<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_buku extends CI_Model {

	public function simpan($foto){
		$data = array(
				'judul_buku'	=> $this->input->post('judul_buku'),
				'tahun'			=> $this->input->post('tahun'),
				'kode_kategori' => $this->input->post('kategori'),
				'harga'			=> $this->input->post('harga'),
				'penerbit'		=> $this->input->post('penerbit'),
				'penulis'		=> $this->input->post('penulis'),
				'stok'			=> $this->input->post('stok'),
				'foto_cover'	=> $foto['file_name']
			);

		$this->db->insert('data_buku',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function edit($id){
		$data = array(
				'judul_buku'	=> $this->input->post('judul_buku'),
				'tahun'			=> $this->input->post('tahun'),
				'kode_kategori'	=> $this->input->post('kategori'),
				'harga'			=> $this->input->post('harga'),
				'penerbit'		=> $this->input->post('penerbit'),
				'penulis'		=> $this->input->post('penulis'),
				'stok'			=> $this->input->post('stok'),
			);

		$this->db->where('kode_buku',$id)->update('data_buku',$data);

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}

	}

	public function getDetailBuku($id)
	{
		return $this->db->get_where('data_buku',array('kode_buku'=>$id));
	}

	public function getDataBuku()
	{
		return $this->db->join('data_kategori_buku', 'data_kategori_buku.kode_kategori = data_buku.kode_kategori')->get('data_buku')->result();
	}

	public function delete($id)
	{
		$this->db->where('kode_buku',$id)
				 ->delete('data_buku');

		if($this->db->affected_rows() > 0){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function getKategori()
	{
		return $this->db->get('data_kategori_buku')->result();
	}

}

/* End of file Model_buku.php */
/* Location: ./application/models/Model_buku.php */