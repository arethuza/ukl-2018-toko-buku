<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_transaksi extends CI_Model {
  
	  public function getDataBukuTrans()
	  {
	    return $this->db->join('data_kategori_buku','data_kategori_buku.kode_kategori=data_buku.kode_kategori')
	    ->where('stok != 0')
	    ->get('data_buku')->result();
	  }


	  public function detail($id)
	  {
	    return $this->db->join('data_kategori_buku','data_kategori_buku.kode_kategori=data_buku.kode_kategori')
	    ->where('kode_buku',$id)
	    ->get('data_buku')
	    ->row();
	  }


	  public function simpanTrans()
	  {
	    $object = array(
	      'kode_user'=>$this->session->userdata('id'),
	      'nama_pembeli' => $this->input->post('nama_pembeli') , 
	      'total' => $this->cart->total(),
	      'tanggal_beli'=>date('Y-m-d') 
	    );

	    $this->db->insert('transaksi', $object);

	    $tm_nota=$this->db->order_by('kode_transaksi','desc')
	              ->where('nama_pembeli',$this->input->post('nama_pembeli'))
	              ->limit(1)
	              ->get('transaksi')
	              ->row();

	    for($i=0;$i<count($this->input->post('kode_buku'));$i++){

	      $hasil[]=array(
	        'kode_transaksi'=>$tm_nota->kode_transaksi,       
	        'jumlah'=>$this->input->post('qty')[$i],
	        'kode_buku'=>$this->input->post('kode_buku')[$i],
	      );

	      $stok = array(
	        'stok' => $this->input->post('stok')[$i]-$this->input->post('qty')[$i],
	      );

	      $this->db->where('kode_buku',$this->input->post('kode_buku')[$i])
	           ->update('data_buku', $stok);

	    }  


	    $proses=$this->db->insert_batch('detail_transaksi', $hasil);

	    if($proses){
	      return $tm_nota->kode_transaksi;
	    } else {
	      return 0;
	    }

	  }


	  public function lastTrans()
	  {
	    return $this->db ->order_by('kode_transaksi','desc')
			             ->where('nama_pembeli',$this->input->post('nama_pembeli'))
			             ->limit(1)
			             ->get('transaksi')
			             ->row();
	  }
}