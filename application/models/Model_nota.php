<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_nota extends CI_Model {

	public function getDataNota($id)
	{
		return $this->db->join('data_buku','data_buku.kode_buku=detail_transaksi.kode_buku')
						->join('data_kategori_buku','data_kategori_buku.kode_kategori=data_buku.kode_kategori')
						->where('kode_transaksi', $id)
						->get('detail_transaksi');
	}
	public function getDataTransaksi($id)
	{
		return $this->db->join('data_user','data_user.kode_user=transaksi.kode_user')
						->where('kode_transaksi', $id)
						->get('transaksi')->row();
	}

}

/* End of file Model_nota.php */
/* Location: ./application/models/Model_nota.php */