# Installation

#### Download and Import the database

## Preview
Website yang saya buat saat UKL kelas 11 pada tahun 2018<br>
untuk templatenya ditentukan oleh guru<br>
dan berikut adalah fitur2nya

#### Login
<img src="https://cdn-images-1.medium.com/max/880/1*Ep58O1prlObh7ZaQkhpbkA.png">

#### Dashboard
<img src="https://cdn-images-1.medium.com/max/800/1*tXnomtCan_l5nlnJBpfcmQ.png">

#### List User
<img src="https://cdn-images-1.medium.com/max/800/1*w47tA7MzRpsbhB30PmHBqQ.png">

#### List Book
<img src="https://cdn-images-1.medium.com/max/800/1*OejLzZViJNoKtQhLBB0hpA.png">

#### List Category
<img src="https://cdn-images-1.medium.com/max/880/1*BdYFYc0x3Ruq2ZEcBP1UVA.png">
